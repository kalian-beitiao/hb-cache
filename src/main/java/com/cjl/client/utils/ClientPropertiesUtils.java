package com.cjl.client.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ClientPropertiesUtils {
    static Properties properties;

    static {
        try (InputStream in = ClientPropertiesUtils.class.getClassLoader().getResourceAsStream("client.properties")) {
            properties = new Properties();
            properties.load(in);
        } catch (IOException e) {
            throw new ExceptionInInitializerError(e);
        }
    }

    public static int getServerPort() {
        String port = properties.getProperty("hbCache.server.port");
        return port == null ? 8080 : Integer.parseInt(port);
    }

    public static String getServerHost(){
        String host = properties.getProperty("hbCache.server.host");
        if(host == null || "".equals(host)){
            return "localhost";
        } else{
            return host;
        }
    }
}
