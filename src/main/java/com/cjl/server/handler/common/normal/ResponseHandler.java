package com.cjl.server.handler.common.normal;

import com.cjl.message.ResponseMessage;
import com.cjl.server.handler.CommonHandler;
import io.netty.channel.ChannelHandlerContext;
import lombok.Synchronized;

public class ResponseHandler implements CommonHandler<ResponseMessage> {
    @Synchronized
    @Override
    public ResponseMessage process(ResponseMessage msg) throws Exception {
        return msg;
    }
}
