package com.cjl.server.handler;

import com.cjl.message.Message;
import com.cjl.message.ResponseMessage;
import io.netty.channel.ChannelHandlerContext;
import lombok.Synchronized;

public interface CommonHandler<T extends Message> {

    ResponseMessage process(T msg) throws Exception;
}
