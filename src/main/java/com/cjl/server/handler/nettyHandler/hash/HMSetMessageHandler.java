package com.cjl.server.handler.nettyHandler.hash;

import com.cjl.constrants.ResultCode;
import com.cjl.message.ResponseMessage;
import com.cjl.message.hashMessage.HGetMessage;
import com.cjl.message.hashMessage.HMSetMessage;
import com.cjl.server.handler.CommonHandler;
import com.cjl.server.handler.HandlerManager;
import com.cjl.server.session.Session;
import com.cjl.server.store.CacheNode;
import com.cjl.server.store.HbCache;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

import java.util.HashMap;
import java.util.Map;

@ChannelHandler.Sharable
public class HMSetMessageHandler extends SimpleChannelInboundHandler<HMSetMessage> {
    @Override
    protected void channelRead0(ChannelHandlerContext ctx, HMSetMessage msg) throws Exception {
        ResponseMessage responseMessage = HandlerManager.process(msg);
        ctx.writeAndFlush(responseMessage);
    }
}
