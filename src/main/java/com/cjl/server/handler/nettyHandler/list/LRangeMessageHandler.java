package com.cjl.server.handler.nettyHandler.list;

import com.cjl.constrants.ResultCode;
import com.cjl.message.ResponseMessage;
import com.cjl.message.hashMessage.HGetMessage;
import com.cjl.message.listMessage.LRangeMessage;
import com.cjl.server.handler.CommonHandler;
import com.cjl.server.handler.HandlerManager;
import com.cjl.server.store.CacheNode;
import com.cjl.server.store.HbCache;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.LinkedBlockingQueue;

@ChannelHandler.Sharable
public class LRangeMessageHandler extends SimpleChannelInboundHandler<LRangeMessage> {
    @Override
    protected void channelRead0(ChannelHandlerContext ctx, LRangeMessage msg) throws Exception {
        ResponseMessage responseMessage = HandlerManager.process(msg);
        ctx.writeAndFlush(responseMessage);
    }
}
