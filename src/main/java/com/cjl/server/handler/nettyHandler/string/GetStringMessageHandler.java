package com.cjl.server.handler.nettyHandler.string;

import com.cjl.constrants.ResultCode;
import com.cjl.message.ResponseMessage;
import com.cjl.message.stringMessage.GetStringMessage;
import com.cjl.server.handler.HandlerManager;
import com.cjl.server.store.CacheNode;
import com.cjl.server.store.HbCache;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

@ChannelHandler.Sharable
public class GetStringMessageHandler extends SimpleChannelInboundHandler<GetStringMessage> {
    @Override
    protected void channelRead0(ChannelHandlerContext ctx, GetStringMessage msg) throws Exception {
        ResponseMessage responseMessage = HandlerManager.process(msg);
        ctx.writeAndFlush(responseMessage);
    }
}
