package com.cjl.server.handler.nettyHandler.normal;

import com.cjl.constrants.CommandConstrants;
import com.cjl.constrants.ResultCode;
import com.cjl.message.HelpMessage;
import com.cjl.message.ResponseMessage;
import com.cjl.server.handler.CommonHandler;
import com.cjl.server.handler.HandlerManager;
import com.cjl.server.session.Session;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

import java.util.List;

@ChannelHandler.Sharable
public class HelpMessageHandler extends SimpleChannelInboundHandler<HelpMessage> {
    @Override
    protected void channelRead0(ChannelHandlerContext ctx, HelpMessage msg) throws Exception {
        ResponseMessage responseMessage = HandlerManager.process(msg);
        ctx.writeAndFlush(responseMessage);
    }
}
