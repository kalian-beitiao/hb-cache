package com.cjl.server.handler.nettyHandler.set;

import com.cjl.constrants.ResultCode;
import com.cjl.message.ResponseMessage;
import com.cjl.message.setMessage.SDiffMessage;
import com.cjl.server.handler.HandlerManager;
import com.cjl.server.store.CacheNode;
import com.cjl.server.store.HbCache;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

import java.util.Set;

@ChannelHandler.Sharable
public class SDiffMessageHandler extends SimpleChannelInboundHandler<SDiffMessage> {
    @Override
    protected void channelRead0(ChannelHandlerContext ctx, SDiffMessage msg) throws Exception {
        ResponseMessage responseMessage = HandlerManager.process(msg);
        ctx.writeAndFlush(responseMessage);
    }
}
