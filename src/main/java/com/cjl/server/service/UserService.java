package com.cjl.server.service;

import java.io.IOException;

public interface UserService {
    boolean login(String username, String password);
}
