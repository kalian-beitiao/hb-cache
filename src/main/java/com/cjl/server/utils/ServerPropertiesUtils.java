package com.cjl.server.utils;

import com.cjl.protocol.Serializer;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ServerPropertiesUtils {

    static Properties properties;

    static {
        try (InputStream in = ServerPropertiesUtils.class.getClassLoader().getResourceAsStream("server.properties")) {
            properties = new Properties();
            properties.load(in);
        } catch (IOException e) {
            throw new ExceptionInInitializerError(e);
        }
    }

    public static String readProperties(String key) {
        String value = properties.getProperty(key);
        return value;
    }

    public static int getServerPort() {
        String port = properties.getProperty("hbCache.server.port");
        return port == null ? 8080 : Integer.parseInt(port);
    }

    public static Serializer.Algorithm getSerializerAlgorithm() {
        String value = properties.getProperty("serializer.algorithm");
        if (value == null) {
            return Serializer.Algorithm.Java;
        } else {
            return Serializer.Algorithm.valueOf(value);
        }
    }

    public static String getUsername(){
        String username = properties.getProperty("server.username");
        return username;
    }

    public static String getPassword(){
        String password = properties.getProperty("server.password");
        return password;
    }
}
