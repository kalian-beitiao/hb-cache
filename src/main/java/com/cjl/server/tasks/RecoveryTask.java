package com.cjl.server.tasks;

import com.cjl.client.HbClient;
import com.cjl.client.handler.CheckCommandHandler;
import com.cjl.constrants.ResultCode;
import com.cjl.message.Message;
import com.cjl.message.ResponseMessage;
import com.cjl.server.handler.HandlerManager;
import com.cjl.server.utils.ServerPropertiesUtils;
import io.netty.channel.Channel;
import lombok.SneakyThrows;

import java.io.*;

public class RecoveryTask implements Runnable{

    private String filePath;

    private CheckCommandHandler checkCommandHandler;

    public RecoveryTask() {
        checkCommandHandler = new CheckCommandHandler();
        filePath = ServerPropertiesUtils.readProperties("server.backup.path");
        if(filePath == null || "".equals(filePath)){
            filePath = "D://backup//";
        }
    }


    @Override
    public void run() {
        File dir = new File(filePath);
        if(dir.isDirectory()){
            File[] files = dir.listFiles();
            for(File file : files){
                try {
                    BufferedReader reader = new BufferedReader(new FileReader(file));
                    String command = reader.readLine();
                    while(command != null){
                        Message message = checkCommandHandler.checkCommand(command, null);
                        try {
                            HandlerManager.process(message);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        command = reader.readLine();
                    }
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
